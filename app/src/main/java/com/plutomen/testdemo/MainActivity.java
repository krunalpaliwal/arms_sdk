package com.plutomen.testdemo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.plutomen.armssdk.Utils;

public class MainActivity extends AppCompatActivity {
    private EditText edtDD;
    private EditText edtMM;
    private EditText edtYYYY;
    private Button btnSubmit;
    private TextView txtAge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        edtDD = (EditText) findViewById(R.id.edtDD);
        edtMM = (EditText) findViewById(R.id.edtMM);
        edtYYYY = (EditText) findViewById(R.id.edtYYYY);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        txtAge = (TextView) findViewById(R.id.txtAge);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dd = edtDD.getText().toString().trim();
                String mm = edtMM.getText().toString().trim();
                String yyyy = edtYYYY.getText().toString().trim();
                String myDate = dd + "/" + mm + "/" + yyyy;
                txtAge.setText("Total Age : " + Utils.calculateAge(myDate));
            }
        });
    }
}